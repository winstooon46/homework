import React from "react";
import Footer from "./Components/Footer/indexs"
import Header from  "./Components/Header/indexs"
import Positions from "./Components/Positions/Positions";
import './App.css'



class App extends React.Component {
    componentDidMount() {
        fetch("nameItem.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {

        return (
            <>
                <div className="appClass">
                    <Header/>
                    <Positions />
                    <Footer/>
                </div>

            </> )
    }
}
export default App