import React, {Component} from 'react';
import Item from "./Item";

export class Positions extends Component {
    render() {
        const {clickModal} = this.props
        return (
         <main>
             {this.props.items.map(el => (
             <Item key= {el.id} item = {el} onAdd={this.props.onAdd} clickModal={clickModal} />
             ))}
         </main>
        )
    }
}



export default Positions


