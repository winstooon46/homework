import React, {useState} from "react";
import '../../../App.css'
import Modal from "../Modal";

const Two = () => {
    const [modalActive,setModalActive]= useState(false)
  return(
      <div className= 'apps'>
        <main>
          <button className="open-btn styleBtn" onClick={() => setModalActive(true)}>Open second modal</button>
        </main>
          <Modal active ={modalActive} setActive ={setModalActive}>
          <h1 className="hedername"> Do you want to delete this file <span className="icon" onClick={()=>setModalActive(false)}>&#10006;</span></h1>
              <p className="text">Once </p>
              <div className="btns">
                  <button className="ok" >Ok</button>
                  <button className="cancel" onClick={()=> setModalActive(false)}>Cancel</button>
              </div>
          </Modal>
      </div>
  )
}
export default Two;