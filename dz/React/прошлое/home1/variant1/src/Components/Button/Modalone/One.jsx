import React, {useState} from "react";
import '../../../App.css'
import Modal from "../Modal";

const One = () => {
    const [modalActive,setModalActive]= useState(false)
  return(
      <div className= 'app'>
        <main>
          <button className="open-btn styleBtn" onClick={() => setModalActive(true)}>Open first modal</button>
        </main>
          <Modal active ={modalActive} setActive ={setModalActive}>
          <h1 className="hedername"> Do you want to delete this file <span className="icon" onClick={()=>setModalActive(false)}>&#10006;</span></h1>
              <p className="text">Once uoy delete this file? it won`t be possoble to undo this action</p>
              <div className="btns">
                  <button className="ok" >Ok</button>
                  <button className="cancel" onClick={()=> setModalActive(false)}>Cancel</button>
              </div>
          </Modal>
      </div>
  )
}
export default One;