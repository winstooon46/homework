import React, {Component} from "react";
import "./stule.css"
class ButtonTwo extends Component{

    render() {
        const {textBtn,onClick} = this.props;
        return(
            <div className= 'app'>

                    <button className="open-btn" type='button' onClick={onClick}>{textBtn}</button>

                </div>
        )
    }
}
export default ButtonTwo;