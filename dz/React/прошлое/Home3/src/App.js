import React from 'react';
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Items from "./Components/Items";




class App extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            items: [],
            showFullItem: false,


        }
        this.addToOrder = this.addToOrder.bind(this)
        this.deleteOrder = this.deleteOrder.bind(this)
        this.onShowItem = this.onShowItem.bind(this)
    }

    componentDidMount() {
        fetch("nameItem.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

 render() {
    return(
<div className="wrapper">
    <Header orders = {this.state.orders} onDelete={this.deleteOrder}/>
    <Items onShowItem = {this.onShowItem} items = {this.state.items} onAdd={this.addToOrder}/>

    <Footer/>
    <div className="buttons">


    </div>
</div>
    )
}

onShowItem(){
        this.setState({showFullItem: !this.state.showFullItem})
}




deleteOrder(id){
    this.setState({orders:this.state.orders.filter(el => el.id !==id)})
}

addToOrder(item){
        let isInArray = false
        this.state.orders.forEach(el => {
            if (el.id === item.id)
                isInArray = true
        })
    if (!isInArray)
this.setState({orders:[...this.state.orders,item]})
}
}

export default App


