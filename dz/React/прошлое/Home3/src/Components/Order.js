import React, {Component}from "react";
import Two from "./Button/ModalTwo/Two";
import {FaTrash} from "react-icons/fa";

export class Order extends Component{
    render() {
        return(
        <div className="item">
            <img src={"./chop/" + this.props.item.img} alt=""/>
            <h2>{this.props.item.title}</h2>
            <b>{this.props.item.price}$</b>
            <Two/>
             <FaTrash className='delete-icon' onClick={()=>this.props.onDelete(this.props.item.id)}/>
        </div>
        )

    }
}
export default Order
