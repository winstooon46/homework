import React, {useState} from 'react'
import {GiShoppingCart} from "react-icons/gi";
import Order from "./Order";

const showOrders = (props) => {
    let summa = 0
    props.orders.forEach(el => summa += Number.parseFloat(el.price))
    return(
        <div>
            {props.orders.map(el => (
                <Order onDelete = {props.onDelete} key = {el.id} item = {el} />
            ))}
            <p className='summa'>Сумма:{new Intl.NumberFormat().format(summa)}$ </p>
        </div>
    )
}
const showNothing = () => {
    return(
        <div className='empty'>
            <h2>Товаров нет</h2>
        </div>
    )
}

export default function Header (props){
    let [cardOpen, setCardOpen] = useState(false)
    return(
       <header>
           <div>
               <span className="logo">House Staff</span>
               <ul className="nav">
                   <li>Про нас</li>
                   <li>Контакты</li>
                   <li>Кабинет</li>
               </ul>
               <GiShoppingCart onClick={() => setCardOpen(cardOpen= !cardOpen)} className={`shop-card-button ${cardOpen && 'active'}`}/>


               {cardOpen && (
                   <div className='shop-card'>
                       {props.orders.length > 0?
                       showOrders(props) : showNothing()}
                       
                   </div>
               )}
           </div>

           <div className="presentation"> </div>
       </header>
    )
}
