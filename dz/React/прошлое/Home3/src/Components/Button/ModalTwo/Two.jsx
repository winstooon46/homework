import React, {useState} from "react";
import '../../../App.css'
import Modal from "../Modal";
import {FaTrash} from "react-icons/fa";

const Two = () => {
    const [modalActive,setModalActive]= useState(false)
  return(
      <div className= 'apps'>
        <main>
          <FaTrash className="delete-icon" onClick={() => setModalActive(true)}> </FaTrash>
        </main>
          <Modal active ={modalActive} setActive ={setModalActive}>
          <h1 className="hedername"> Хотите удалить товар из корзины? <span className="icon" onClick={()=>setModalActive(false)}>&#10006;</span></h1>
              <div className="btns">
                  <button className="ok" onClick={()=>this.props.onDelete(this.props.item.id)}>Yes</button> {/* Вот в этом проблема,*/}
                  <button className="cancel" onClick={()=> setModalActive(false)}>Cancel</button>
              </div>
          </Modal>
      </div>
  )
}
export default Two;