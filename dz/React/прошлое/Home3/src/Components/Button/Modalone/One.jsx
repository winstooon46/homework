import React, {useState} from "react";

import '../../../App.css'
import Modal from "../Modal";


const One = () => {
    const [modalActive,setModalActive]= useState(false)
  return(
      <>
      <div className= 'app'>
        <main>
          <button className="add-to-card"  onClick={() => setModalActive(true)}>+</button>
        </main>
      </div>
      <div className="modal-window">
    <Modal active ={modalActive} setActive ={setModalActive}>
        <h1 className="hedername"> Хотите добавить товар в корзину? <span className="icon" onClick={()=>setModalActive(false)}>&#10006;</span></h1>
        <div className="btns">

            <button className="ok" onClick={() => this.props.onAdd(this.props.item)}>Yes</button>
            <button className="cancel" onClick={()=> setModalActive(false)}>Cancel</button>
        </div>
    </Modal>
      </div>
      </>
  )
}
export default One;