import React, {Component} from "react";
import One from "./Button/Modalone/One";
import {AiFillStar} from "react-icons/ai";

export class Item extends Component{
    render() {
        return(
            <div className='item'>
                <img src={"./chop/" + this.props.item.img} alt=""/>
                <h2>{this.props.item.title}  <AiFillStar className="iconStars"/> </h2>
                <p>{this.props.item.desc}</p>
                <b>{this.props.item.price}$</b>
                <One/>
                <div className='add-to-card' onClick={() => this.props.onAdd(this.props.item)}>+</div>
             </div>
        )
    }
}
export default Item
