import './App.css';
import {Component} from "react";
import Modal from "./Components/Modal/";
import Header from "./Components/Header/index";
import Positions from "./Components/Positions/index"



class App extends Component {
state = {
    isModalOne: false,
    isModalTwo: false,
    iconStar: false,
    items: [],
    favorites: [],    //корзинка
    favoritesTrol: {},  //корзинка
    favoritStar: [],
    currentStar: {}
}

    componentDidMount() {
        fetch("nameItem.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items
                    });
                },
            )
    }
    hanrlerModalOne = () => {
    this.setState((prevState) => {
        return{
            ...prevState,
            isModalOne: !prevState.isModalOne

        }
    })
}
    hanrlerModalTwo = () => {
        this.setState((prevState) => {
            return{
                ...prevState,
                isModalTwo: !prevState.isModalTwo
            }
        })
    }

    //     handlerFavorites = (favoritesTrol) => {
    //         localStorage.setItem('favoritesTrol',JSON.stringify(favoritesTrol))
    //
    //     this.setState((prevState) => {
    //         return {
    //             ...prevState,
    //             isModalOne: !prevState.isModalOne,
    //             favorites: [...prevState.favorites, favoritesTrol]
    //
    //         }
    //     })
    // }

    handlerFavoritesStar = (currentStar) => {
    /*Переписал вам логику стар сделал проверки и сключение */
        localStorage.setItem('currentStar',JSON.stringify(this.state.favoritStar))

        if (!this.state.favoritStar.includes(currentStar)){
            this.setState((prevState) => {
                return {
                    ...prevState,
                    favoritStar: [...prevState.favoritStar, currentStar]

                }
            })
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    favoritStar: [...this.state.favoritStar.filter(item => item !== currentStar)]

                }
            })
        }
    }
    render (){
    const {isModalOne,isModalTwo,favorites,favoritesTrol,favoritStar,currentStar} = this.state;
        console.log(favoritStar);
        return(

            <div>
                <Header
                    count={favorites.length}
                    counted={favoritStar.length}
                    openModalTwo={this.hanrlerModalTwo}  />
                {isModalTwo && <Modal   modalTwo={'Ваши действия'}
                                        textHeaderTwo={' Желаете удалить товар из корзины?'}
                                        closeModal={this.hanrlerModalTwo}  />}

                <Positions
                    openModalOne={this.hanrlerModalOne}
                    items={this.state.items}
                    itemsFavorites={this.state.favoritStar}
                    hendStar={this.handlerFavoritesStar}/>

                {isModalOne && <Modal modalOne={'Ваши действия?'}
                                      textHeaderOne={' Желаете добавить товар в корзину?'}
                                      closeModal={this.hanrlerModalOne}
                                      handlerModal={() => this.handlerFavorites(favoritesTrol)}
                />}
            </div>
        )
    }
}

export default App;


