import React, {Component} from "react";
import Items from "./Items";


class Positions extends Component{
    render() {
        const {openModalOne,hendStar,itemsFavorites} = this.props
        return (
            <main>
                {this.props.items.map(el => (
                    <Items key= {el.id} item = {el}  openModalOne={openModalOne} itemsFavorites={itemsFavorites} favoritStars={hendStar} />
                ))}
            </main>
        )
    }
}

export default Positions