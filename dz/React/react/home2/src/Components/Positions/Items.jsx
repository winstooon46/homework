import React, {Component} from 'react';
import "./Positions.scss"
import { AiOutlineStar,AiFillStar } from 'react-icons/ai';
import PropTypes from "prop-types";


class Items extends Component {

state = {
    basket: []
}
    handlerFavorites = (basket) => {
        localStorage.setItem('favoritesTrol',JSON.stringify(basket))

        this.setState((prevState) => {
            return {
                ...prevState,
                isModalOne: !prevState.isModalOne,
                favorites: [...prevState.favorites, basket]

            }
        })
    }

    render() {

        const {openModalOne, favoritStars, itemsFavorites} = this.props

        return (
            <div className='item'>
                <img src={"./chop/" + this.props.item.img} alt=""/>
                <h2>{this.props.item.title}  </h2>
                <p>{this.props.item.desc}</p>
                <b>{this.props.item.price}$</b>
                {/*Тут нужно было принять функцию и отдать какой то парамет что бы понимать какую карточку вы добавили в вишлист или весь обьект this.props.item или просто id что бы наполнить ваш массив, я передал ид по этому делаю инклуд*/}
                <div className= "iconItem" onClick={()=> favoritStars(this.props.item.id)}>
                    {!itemsFavorites.includes(this.props.item.id) ?  <AiOutlineStar/> : <AiFillStar/>  }
                </div>
                <div className="add-to-card active" onClick={openModalOne} >Добавить в корзину?</div>

             </div>
        );
    }
}
Items.propTypes = {
    openModalOne: PropTypes.func
};

export default Items;


