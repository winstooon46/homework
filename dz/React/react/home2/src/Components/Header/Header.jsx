import { BsFillBasket2Fill } from 'react-icons/bs';
import PropTypes from 'prop-types';
import './Header.scss'
import React, {Component} from "react";
import { IoIosStarOutline } from 'react-icons/io';

// GiShoppingCart import {GiShoppingCart} from "react-icons/gi";


class Header extends Component{


    render () {
        const {openTrollCard,count,counted} = this.props    // добавить openModalTwo

        return(
            <header>
                <div className="headerUp">
                    <p className="logo">Мои Мечи </p>
                    <div className="favorites">
                        <p className="starBasket">Избранное</p>
                        <IoIosStarOutline className="star" />
                        <span className="counts ">{counted}</span>
                    </div>

                    <div className="icon-favorite" onClick={openTrollCard}>
                        <p className="basket">Корзина</p>
                        <BsFillBasket2Fill className="troll">
bg
                        </BsFillBasket2Fill>
                        <span className="count ">{count}</span>
                    </div>


                   {/*<BsFillBasket2Fill className="troll" onClick={openTrollCard}/>*/}
                        {/*<button className="button button2" type="button" onClick={openModalTwo} >Кашолка</button>*/}

                </div>
                <div className="presentation"> </div>
            </header>

        )
    }
}



Header.propTypes = {
    count: PropTypes.number
};
export default Header