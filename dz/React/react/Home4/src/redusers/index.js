import {createReducer, isAsyncThunkAction} from "@reduxjs/toolkit";
import * as actions from "../actions"


const initialState ={
    positions: [],
    isModalOne: false,
    isModalTwo: false,
    // basket: [],
    // favorites: [],
    // pageBasket: {},
    // pageFavorites: {}
};


export default createReducer(initialState, builder =>{
    builder
        .addCase(actions.actionsIsModalOne, (state) => {
            state.isModalOne = !state.isModalOne
        })
        .addCase(actions.actionsIsModalTwo, (state) => {
            state.isModalTwo = !state.isModalTwo
        })
        .addMatcher(isAsyncThunkAction(actions.actionFetchPositions), (state, {payload,meta}) => {
            if(meta.requestStatus === 'fulfilled'){
                state.positions = [...payload.items];
            }
        })

        // .addMatcher(isAsyncThunkAction(actions.actionFetchBasket),(state, {payload}) => {
        // const {result} = payload;
        // state.basket = [...payload.result];
        // })
        // .addMatcher(isAsyncThunkAction(actions.actionFetchFavorites),(state, {payload,meta}) => {
        //     const {result} = payload;
        //     state.favorites = [...payload.result];
        // })
        // .addMatcher(isAsyncThunkAction(actions.actionFetchPageBasket),(state, {payload}) => {
        // const {result} = payload;
        // state.pageBasket = {...result};
        // })
        // .addMatcher(isAsyncThunkAction(actions.actionFetchPageFavorites),(state, {payload}) => {
        // const {result} = payload;
        // state.pageFavorites = {...result};
        // })
})