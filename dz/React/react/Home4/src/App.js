import './App.css';
import Header from "./Components/Header/index";
import {Routes, Route } from "react-router-dom";
import FavoritesStar from "./Components/pages/FavoritesStar/index";
import FavoritesTrol from "./Components/pages/FavoriyesTrol/index";
import Positions from "./Components/Positions";
import {useSelector} from "react-redux";
import {selectorPositions} from "./selector";



const App = () =>  {

    const items = useSelector(selectorPositions)

    return(
        <>
            <Header/>

            <Routes>
                <Route path="/" element={
                    <Positions/>}/>
                <Route path="/favorites" element ={<FavoritesStar itemList={items}/>}/>
                <Route path="/basket" element ={<FavoritesTrol itemList={items}/>}/>
            </Routes>

        </>
    )
}
export default App