import {configureStore} from "@reduxjs/toolkit";
import rootReducers from "../redusers";



const store = configureStore({
 reducer: rootReducers
});

 export default store;