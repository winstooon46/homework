import {createAction,createAsyncThunk} from "@reduxjs/toolkit";
import {sendRequest} from "../helpers";


export const actionPositions = createAction('ACTION_POSITIONS')
export const actionsIsModalOne = createAction('ACTION_IS_MODAL_ONE')
export const actionsIsModalTwo = createAction('ACTION_IS_MODAL_TWO')
// export const actionBasket = createAction('ACTION_BASKET');
// export const actionFavorites = createAction('ACTION_FAVORITES');
// export const actionPageBasket = createAction('ACTION_PAGE_BASKET');
// export const actionPageFavorites = createAction('ACTION_PAGE_FAVORITES');

export const actionFetchPositions = createAsyncThunk("positions/fetchData",
    async() =>  {
        const result = await sendRequest("nameItem.json")
        return result

    })

// export const actionFetchBasket = createAsyncThunk("basket/fetchData",
//     async() =>  {
//      const result = await sendRequest("nameItem.json")
//         return result
// })
// export const actionFetchFavorites = createAsyncThunk("favorites/fetchData",
//     async() =>  {
//                  const result = await sendRequest("nameItem.json")
//                     return result
// })
// export const actionFetchPageBasket = createAsyncThunk("pageBasket/fetchData",
//     async() =>  {
//      const result = await sendRequest("nameItem.json")
//         return result
// })
// export const actionFetchPageFavorites = createAsyncThunk("pageFavorites/fetchData",
//     async() =>  {
//      const result = await sendRequest("nameItem.json")
//         return result
// })
