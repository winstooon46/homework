
import "./Positions.scss"
import Items from "./Items";
import {useEffect} from "react";
import{actionFetchPositions} from "../../actions";
import {selectorPositions} from "../../selector";
import {useSelector,useDispatch} from "react-redux";





function Positions() {

    const itemList = useSelector(selectorPositions)
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(actionFetchPositions())
    },[])

    return (
        <main>
            {itemList.map(el => (
                <Items key={el.id} item = {el} />
            ))}

        </main>
    );
}



export default Positions