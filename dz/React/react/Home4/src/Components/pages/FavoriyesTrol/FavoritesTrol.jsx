
import "./FavoritesTrol.scss"
import {TiDelete} from "react-icons/ti";
import {useEffect, useState} from "react";
import Modal from "../../Modal";

import {useSelector,useDispatch} from "react-redux";
import {actionsIsModalTwo} from "../../../actions";
import {selectorIsModalTwo} from "../../../selector";

const FavoritesTrol = ({itemList}) => {

    const [goodsInBasket, setItemInBasket] = useState([]);

    const isModalTwo = useSelector(selectorIsModalTwo)
    const dispatch = useDispatch()





    //Удалиние из корзинки

    const removItemFromBasket = (curr_id) => {
        const updateBasket = goodsInBasket.filter(p=> p.id !== curr_id)
        const id_list = [];
        updateBasket.forEach(item => id_list.push(item.id))

        setItemInBasket(updateBasket)
        localStorage.setItem('favorites',JSON.stringify(id_list))
    }


    const hanrlerModalTwo = () => {
        dispatch(actionsIsModalTwo())
    }

    const getBasketList = (basketList) => {
        const new_list = []
        if (itemList.length){
            itemList.forEach(item => {
                basketList.forEach(item_id => {
                    if (item.id === item_id) {
                        new_list.push(item)
                    }
                })
            })
        }
        return new_list;
    }


    useEffect(()=>{
        const goods = JSON.parse(localStorage.getItem("favorites"))
        if (goods) {
            setItemInBasket(getBasketList(goods))
        }
    }, [itemList])





return (
    <div className="basketPage">
    {goodsInBasket.map((favorites, index)=> (

            <div className="itemBasket" key= {index} >
                <TiDelete className="buttonClick" onClick={hanrlerModalTwo}/>

                {isModalTwo && <Modal closeModal={hanrlerModalTwo}
                                      modalOne={'Ваши действия?'}
                                      textHeaderOne={' Желаете удалить товар из корзины?'}
                                      handlerModalBasket={() => removItemFromBasket(favorites.id)}

                />}
                <img src={"./chop/" + favorites.img} alt=""/>
                <h2>{favorites.title}</h2>
                <b>{favorites.price}$</b>
                <button className="buy" >Купить</button>
            </div>
        ))}
    </div>

)
}
export default FavoritesTrol
