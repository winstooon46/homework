import "./Modal.scss"
import PropTypes from "prop-types";



const Modal = ({closeModal,modalOne,textHeaderOne,handlerModalBasket}) => {

    return(
        <div className="modal-wrapper" onClick={closeModal}>
            <div className="modal" onClick={(e)=> e.stopPropagation()}>
                <div className="modal-box" >
                    <button type="button" className="modal-close" >
                        <h1 className="hedername"> {textHeaderOne}<span className="icon" onClick={closeModal}>&#10006;</span></h1>
                    </button>
                    <div className="modal-header">
                        <p>{modalOne}</p>
                    </div>
                    <div className="modal-footer">
                        <div className="button-wrapper">
                            <button className="btn" type="button" onClick={() =>{
                                handlerModalBasket()
                                closeModal()}}>OK</button>
                            <button className="btn" type="button" onClick={closeModal}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propType ={
    closeModal:PropTypes.func,
    textHeaderOne:PropTypes.string,
    textHeaderTwo:PropTypes.string
}
    export default Modal
