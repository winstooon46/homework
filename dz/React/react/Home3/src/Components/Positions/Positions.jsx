
import "./Positions.scss"
import Items from "./Items";



function Positions({itemList}) {



    return (
        <main>
            {itemList.map(el => (
                <Items key={el.id} item = {el} />
            ))}
        </main>
    );
}



export default Positions