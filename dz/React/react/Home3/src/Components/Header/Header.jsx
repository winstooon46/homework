import { BsFillBasket2Fill } from 'react-icons/bs';
// import PropTypes from 'prop-types';
import './Header.scss'
import { IoIosStarOutline } from 'react-icons/io';
import {Link } from "react-router-dom";
import {useState,useEffect} from "react";


    function Header({count,counted}) {


    const [countBasket, setCountBasket] = useState(0);
    const [countStar, setCountStar] = useState(0);

    // useEffect( () => {
    //     localStorage.getItem("favoritStar")
    //     localStorage.getItem("favorites")
    //     setCountStar(localStorage.getItem("favoritStar"))
    //     setCountBasket(localStorage.getItem("favorites"))
    // },[countBasket,countStar])



    const renderBasket = () => {
        return count || countBasket
    }
    const renderStar = () => {
        return counted || countStar
    }
    useEffect(()=>{
        const basket_count = JSON.parse(localStorage.getItem("favorites")) || []
        setCountBasket(basket_count.length)

        const star_count = JSON.parse(localStorage.getItem("favoritStar")) || []
        setCountStar(star_count.length)

    }, [countBasket,countStar])


    return(

        <header>
            <div className="headerUp">
                <Link  to="/" className="logo">Мои Мечи </Link>

                <Link to = "/favorites" className="favorites">
                    <p className="starBasket">Избранное</p>
                    <IoIosStarOutline className="star" />
                    <span className="counts ">{renderStar()}</span>
                </Link>

                <Link  to ="/basket" className="icon-favorite" >
                    <p className="basket">Корзина</p>
                    <BsFillBasket2Fill className="troll" />
                    <span className="count ">{renderBasket()}</span>
                </Link >

            </div>
            <div className="presentation"> </div>
        </header>

    )
}





// Header.propTypes = {
//     count: PropTypes.number
// };
export default Header