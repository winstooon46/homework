import './App.css';
import Header from "./Components/Header/index";

import {Routes, Route } from "react-router-dom";
import FavoritesStar from "./Components/pages/FavoritesStar/index";
import FavoritesTrol from "./Components/pages/FavoriyesTrol/index";

import {useState} from "react";
import Positions from "./Components/Positions";

import {useEffect} from "react";



const App = () =>  {

    // const [isModalOne, setIsModalOne] = useState(false);
    // const [favoritesBasket, setFavoritesBasket] =useState([]);
    const [items, setItem] = useState([]);



    useEffect(()=>{
        fetch("nameItem.json")
            .then(res=> res.json())
            .then(
                (result) =>{
                    setItem(result.items)
                },
            )
    },[])


    // const handlerModalOne = () => {
    //     setIsModalOne(!isModalOne)
    // }


    // const handlerFavorites = (currentBasket) =>{
    //
    //     const getDataFromLocal = JSON.parse(localStorage.getItem('favorites')) || []
    //     let updateBasket = null;
    //     // console.log(currentBasket)
    //     if (getDataFromLocal.includes(currentBasket)){
    //         updateBasket = getDataFromLocal.filter(item => item !== currentBasket)
    //     }
    //     updateBasket = updateBasket || [...getDataFromLocal,currentBasket];
    //     localStorage.setItem('favorites',JSON.stringify(updateBasket))
    //     setFavoritesBasket(updateBasket)
    //     // handlerModalOne()
    // }



    // const handlerFavoritesTroll = (favoritesBasket) => {
    //     setFavoritesBasket({...favoritesBasket})
    // }



            return(
                <>
                    <Header />
                    <Routes>
                        <Route path="/" element={
                            <Positions itemList={items}/>}/>

                            <Route path="/favorites" element ={<FavoritesStar itemList={items}  />}/>
                            <Route path="/basket" element ={<FavoritesTrol itemList={items}  />}/>

                    </Routes>


                    {/*{isModalOne && <Modal closeModal={handlerModalOne}*/}
                    {/*                      modalOne={'Ваши действия?'}*/}
                    {/*                      textHeaderOne={' Желаете добавить товар в корзину?'}*/}
                    {/*                      handlerModalBasket={() => handlerFavorites()} />}*/}
                </>

            )
}
export default App