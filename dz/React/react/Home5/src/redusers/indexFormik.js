import {createReducer} from '@reduxjs/toolkit';
import * as actions from '../actions';
export const defaultState = {
    cv: {
        name: '',
        firstName: '',
        age: '',
        street: '',
        phone: ''
    },
    loading: true
};
export default createReducer(defaultState,{
    [actions.actionCVData]: (state,action) => {
        state.cv = action.payload
    }
})
