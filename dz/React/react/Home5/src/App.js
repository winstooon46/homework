import './App.css';
import Header from "./Components/Header/index";
import {Routes, Route } from "react-router-dom";
import FavoritesStar from "./Components/pages/FavoritesStar/index";
import FavoritesTrol from "./Components/pages/FavoriyesTrol/index";
import Positions from "./Components/Positions";
import {useDispatch, useSelector} from "react-redux";
import {selectorPositions} from "./selector";
import {actionFetchPositions} from "./actions";

import {FormikPage} from "./Components/pages/FormikPage/index";
import {useEffect} from "react";

const App = () =>  {


    const items = useSelector(selectorPositions)
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(actionFetchPositions())
    },[])

            return(
                <>
                    <Header/>

                    <Routes>
                        <Route path="/" element={
                            <Positions itemList={items} />}/>
                        <Route path="/favorites" element ={<FavoritesStar itemList={items}  />}/>
                        <Route path="/basket" element ={<FavoritesTrol itemList={items} />}/>
                        <Route path="/FormikPage" element = {<FormikPage itemList={items}/>}/>
                    </Routes>

                </>
            )
}
export default App