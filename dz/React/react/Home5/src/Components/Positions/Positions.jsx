
import "./Positions.scss"
import Items from "./Items";






function Positions({handlerModalOne, handlerFavoritesTroll,itemList}) {



    return (
        <main>
            {itemList.map(el => (
                <Items key={el.id} item = {el} handlerModalOne={handlerModalOne}  handlerFavoritesTroll={handlerFavoritesTroll}/>
            ))}

        </main>
    );
}



export default Positions