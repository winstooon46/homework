import {AiFillStar, AiOutlineStar} from "react-icons/ai";
import PropTypes from "prop-types";
import {useState,useEffect} from "react";
// import {useDispatch, useSelector} from "react-redux";
// import {selectorIsModalOne} from "../../selector";
// import {actionsIsModalOne} from "../../actions";
import Modal from "../Modal";


function Items ({item})  {
    // const isModalOne = useSelector(selectorIsModalOne)
    // const dispatch = useDispatch()
    const [isModalOne, setIsModalOne] = useState(false);
    const [favoritStar, setFavoriteList] = useState([]);
    const [favoritesBasket, setFavoritesBasket] =useState([]);

    useEffect( ()=>{
        let savedFavorite =  localStorage.getItem("favoritStar");
        if(savedFavorite) {
            setFavoriteList(savedFavorite);
        }
    }, [])

    // const handlerModalOne = () => {
    //     dispatch(actionsIsModalOne())
    // }
    const handlerModalOne = () => {
        setIsModalOne(!isModalOne)
    }

    const handlerFavoritStar = (currentStar) => {

        const getFaforitesFromLocal = JSON.parse(localStorage.getItem("favoritStar")) || []
        let updateFavorites = null;
        if (getFaforitesFromLocal.includes(currentStar)) {
            updateFavorites = getFaforitesFromLocal.filter(item => item !== currentStar)
        }
        updateFavorites = updateFavorites || [...getFaforitesFromLocal, currentStar];
        localStorage.setItem("favoritStar", JSON.stringify(updateFavorites))
        setFavoriteList(updateFavorites)
    }

    const handlerFavorites = (currentBasket) =>{

        const getDataFromLocal = JSON.parse(localStorage.getItem('favorites')) || []
        let updateBasket = null;
        if (getDataFromLocal.includes(currentBasket)){
            updateBasket = getDataFromLocal.filter(item => item.id !== currentBasket)
        }
        updateBasket = updateBasket || [...getDataFromLocal,currentBasket];
        localStorage.setItem('favorites',JSON.stringify(updateBasket))
        setFavoritesBasket(updateBasket)

    }

    return (
        <>
            <div className='item' >
                <img src={"./chop/" + item.img} alt=""/>
                <h2>{item.title}  </h2>
                <p>{item.desc}</p>
                <b>{item.price}$</b>

                <div className="iconItem" onClick={() => handlerFavoritStar(item.id) }>
                    {!favoritStar.includes(item.id) ? <AiOutlineStar/> : <AiFillStar/> }

                </div>

                <div className="add-to-card active" onClick={() => {
                    // handlerFavoritesTroll(item)
                    handlerModalOne()

                }}>Добавить в корзину?
                    {!favoritesBasket.includes(item.id)}

                    {isModalOne && <Modal closeModal={handlerModalOne}
                                          modalOne={'Ваши действия?'}
                                          textHeaderOne={' Желаете добавить товар в корзину?'}
                                          handlerModalBasket={() => handlerFavorites(item.id)} />}
                </div>
            </div>
        </>
    )
}



Items.propTypes = {
    openModalOne: PropTypes.func
};
export default Items