import {Formik, Form} from "formik";
import {useDispatch, useSelector} from "react-redux";

import {selectorCvData} from "../../../selector";
import {actionCVData} from "../../../actions";

import {Input} from '../../Form';
import Button from "../../Button";
import {validationFormik} from './validation'

import './FormikPage.scss';
import {useEffect, useState} from "react";




const FormikPage = ({itemList}) => {
    const dispatch = useDispatch();
    const cvData = useSelector(selectorCvData);
    const [goodsInBasket, setItemInBasket] = useState([]);

    const getBasketList = (basketList) => {
        const new_list = []
        if (itemList.length){
            itemList.forEach(item => {
                basketList.forEach(item_id => {
                    if (item.id === item_id) {
                        new_list.push(item)
                    }
                })
            })
        }
        return new_list;
    }

    const removItemFromBasket = (curr_id) => {
        const updateBasket = goodsInBasket.filter(p=> p.id !== curr_id)
        const id_list = [];
        updateBasket.forEach(item => id_list.push(item.id))

        setItemInBasket(updateBasket)
        localStorage.setItem('favorites',JSON.stringify(id_list))
        localStorage.removeItem('favorites')
        console.log(JSON.stringify(updateBasket, null, 2));
    }

    useEffect(()=>{
        const goods = JSON.parse(localStorage.getItem("favorites"))
        if (goods) {
            setItemInBasket(getBasketList(goods))

        }
    }, [itemList])

    // const removItemFromBasket = () => {
    //     console.log(JSON.parse(JSON.stringify(localStorage.getItem('favorites'), null, 2)))
    //     localStorage.removeItem('favorites')
    //
    // }



    return (
        <div className="page__dashboard">
            <div className="page">
                <Formik
                    initialValues={{...cvData}}
                    onSubmit={(values,{resetForm} ) => {
                        dispatch(actionCVData(values));

                        console.log(JSON.stringify(values, null, 2));

                        resetForm({
                            values:{
                            name: '',
                            firstName: '',
                            age: '',
                            street: '',
                            phone: ''}
                        })

                    }}
                    validationSchema={validationFormik}
                >
                    {({errors, touched, getFieldProps}) => (
                        <Form>
                            <fieldset className="form-block">
                                <legend>CheckOut</legend>
                                <div className="row">
                                    <div className="col">
                                        <Input className={'mb-3'} inputName={'name'} label={'Name'} placeholder='Name'
                                               error={errors.name && touched.name}/>
                                        <Input className={'mb-3'} inputName={'firstName'} label={'FirstName'} placeholder='FirstName'
                                               error={errors.firstName && touched.firstName}/>
                                        <Input inputName={'age'} label={'Age'} placeholder="Age, 18+"
                                               error={errors.age && touched.age}/>
                                        <Input inputName={'street'} label={'Street'} placeholder="Ukraine,Kiev, Sadova 52 "
                                               error={errors.street && touched.street}/>
                                        <Input inputName={'phone'} label={'Phone'} placeholder="+380 (66) 614 36 96"
                                               error={errors.phone && touched.phone}/>
                                    </div>
                                </div>
                                <div className="col-12 buttonSubmit">
                                    <Button className="buttonSubmit" type="submit" isPrimary onClick={removItemFromBasket}>CheekOut</Button>
                                </div>
                            </fieldset>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default FormikPage;
