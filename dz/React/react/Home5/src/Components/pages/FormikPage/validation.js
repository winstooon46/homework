import * as yup from 'yup'


export const validationFormik = yup.object({
    name: yup
        .string('Enter your name')
        .required('Name is required')
        .min(2, "Name is too short"),
    firstName: yup
        .string('Enter your FirstName')
        .required('FirstName is required')
        .min(3, "FirstName is too short"),
    age: yup
        .number('Enter your Age')
        .required('Age is required'),

    street: yup
        .string('Enter your street')
        .required('Street is required'),

    phone: yup
        .number('Enter your phone')
})