import "./FavoritesStar.scss"
import {useEffect, useState} from "react";





const FavoritesStar = ({itemList}) => {

    const [goodsInStar, setGoodsInStar]= useState([]);

    const getFavoriteList = (favoritesList) => {
        const new_list = []
        if (itemList.length){
            itemList.forEach(item => {
                favoritesList.forEach(item_id => {
                    if (item.id === item_id) {
                        new_list.push(item)
                    }
                })
            })
        }

        return new_list;
    }




    useEffect(() => {
        const itemStar = JSON.parse(localStorage.getItem("favoritStar")) // [1,2,3]
        if (itemStar) {
            setGoodsInStar(getFavoriteList(itemStar))



        }
    }, [itemList])




    return(
        <div className="starPage">
            {goodsInStar.map((item, index)=> (
                <div className="itemStar" key= {index} >
                    <img src={"./chop/" + item.img} alt=""/>
                    <h2>{item.title}  </h2>
                    <b>{item.price}$</b>
                </div>
            ))}
        </div>
    )
}

export default FavoritesStar;