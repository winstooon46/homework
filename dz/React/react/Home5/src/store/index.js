import {configureStore} from "@reduxjs/toolkit";

import thunk from "redux-thunk";
import rootReducers from "../redusers/index";


const store = configureStore({
 reducer: rootReducers,
 middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
    devTools: true,

});

 export default store;