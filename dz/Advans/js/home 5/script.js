const API = "https://ajax.test-danit.com/api/json/"


class Card {
    constructor(title, body, name , email, id) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.id = id;
        this.renderCard();
    }

    createPost() {
        const card = document.createElement('div');
        card.classList.add(`twitter-card`);
        card.dataset.id = this.id;
        card.innerHTML = `
      <h2 class="card-title">${this.name}</h2>
      <p class="card-text">${this.email}</p>
      <h4 class="card-text">${this.title}</h4>
      <p class="card-text">${this.body}</p>
      <button class="delete-button">Delete</button>
      `;
        card.querySelector('.delete-button').addEventListener('click', () => {
            this.deleteCard();
        });
        return card;
    }

    renderCard() {
        const container = document.querySelector('.container');
        container.appendChild(this.createPost());
    }


    deleteCard() {
        fetch(`${API}posts/${this.id}`, {
            method: 'DELETE',
        }).then(response => {
            if (response.ok) {
                const card = document.querySelector(`[data-id="${this.id}"]`);
                card.remove();
            } else {
                console.log('Error');
            }
        });
    }
}

async function getData(url) {
    const fetchedUrl = await fetch(url, {
        method: 'GET',
    });
    const data = await fetchedUrl.json();
    return data;
}

async function renderPosts() {
    const postData = await getData(`${API}posts`);
    const array = await postData.map(post => {
        const userById = getData(`${API}users/${post.userId}`);
        return userById;
    });
    const userData = await Promise.all(array);
    postData.forEach(post => {
        new Card(post.title, post.body, userData[post.id-1].name, userData[post.id-1].email, post.id);

    })
}


renderPosts()

