const getIP = 'http://api.ipify.org/?format=json';
const info = 'http://ip-api.com/';
const root = document.querySelector('#addd');



const sendRequest = async (url, method = "GET", config) => {
    return await fetch(url, {
        method,
        ...config
    }).then(response => {
        if(response.ok){
            return response
        }
    })
}


const render = async (url) => {
    const responseUser  = await sendRequest(url)
     const resultUser = await responseUser.json();

    const userIP = await resultUser.ip;


    document.querySelector("#submit").onclick = function() {

        let div = document.createElement('div')
        div.innerHTML =  `<span class="item-name">Ip</span> ${userIP}`
        root.appendChild(div)

        sendRequest (`http://ip-api.com/json/${userIP}?fields=1048597`)
            .then((data)=> data.json())
            .then(ip_data => renderIpData(ip_data))
    }
    }


function renderIpData (ip_data){
    const ul = document.createElement('div')
    for (const info in ip_data) {
        let div = document.createElement('div')
        div.innerHTML =  `<span class="items-name">${info}</span> ${ip_data[info]}`
        // const li = document.createElement('li')
        // li.innerText =` ${info} - ${ ip_data[info]}`

        ul.appendChild(div)
    }
    root.appendChild(ul)
}


render(getIP)


