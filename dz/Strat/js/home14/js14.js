const btn = document.querySelector('.btn-toggle');

btn.addEventListener('click', function() {
    if(!document.body.classList.contains('dark-theme')){
        document.body.classList.add('dark-theme')
        localStorage.setItem('back', 'dark-theme')
    }else{
        document.body.classList.remove('dark-theme')
        localStorage.removeItem('back')
    }
});


document.addEventListener("DOMContentLoaded", function() {
    document.body.classList.add( localStorage.getItem('back'))
});


