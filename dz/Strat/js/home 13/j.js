const wrapper = document.querySelector(".images-wrapper");
const button_stop = document.querySelector('.button_stop')
const button_start = document.getElementsByClassName('button_start')[0]

let timerId = setInterval(startShowing, 3000)
function startShowing (){
        let activeImag = Array.from(wrapper.children).findIndex(item => {
            const index = item.classList.contains('active')
            item.classList.remove('active')
            return index
        })
        if (activeImag === 3 ){
            activeImag = -1
        }
        wrapper.children[activeImag + 1].classList.add('active')
}

button_stop.addEventListener("click", () => clearInterval(timerId))
button_start.addEventListener("click", () => timerId = setInterval(startShowing, 3000))



// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout позволяет вызвать функцию один раз через определённый интервал времени.
// setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//     Задержка перед запуском в миллисекундах (1000 мс = 1 с). Значение по умолчанию – 0.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//     Вызов setTimeout возвращает «идентификатор таймера» timerId, который можно использовать для отмены дальнейшего выполнения.
//     В коде ниже планируем вызов функции и затем отменяем его (просто передумали). В результате ничего не происходит.