const tabsBtn = document.querySelectorAll(".item-slide");
const tabsItems = document.querySelectorAll(".tabs__item");

tabsBtn.forEach(function (item) {
    item.addEventListener("click", function () {
        let currentBtn = item;
        let tabId = currentBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(tabId);
        tabsBtn.forEach(function (item) {
            item.classList.remove('active');
        });
        tabsItems.forEach(function (item) {
            item.classList.remove('active');
        });
        currentBtn.classList.add('active');
        currentTab.classList.add('active');
    });
});


const imag = [
    [{
        data: 'Web Design',
        src: 'images/gridfoto/1.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/2.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/3.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/4.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/5.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/6.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/7.jpg'
    }, {
        data: 'Web Design',
        src: 'images/gridfoto/8.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/9.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/10.jpg'
    }, {
        data: 'Web Design',
        src: 'images/gridfoto/11.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/12.jpg'
    }],
    [{
        data: 'Web Design',
        src: 'images/gridfoto/1.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/2.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/3.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/4.jpg'
    }, {
        data: 'Landing Pages',
        src: 'images/gridfoto/5.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/6.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/7.jpg'
    }, {
        data: 'Web Design',
        src: 'images/gridfoto/8.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/9.jpg'
    }, {
        data: 'Wordpress',
        src: 'images/gridfoto/10.jpg'
    }, {
        data: 'Web Design',
        src: 'images/gridfoto/11.jpg'
    }, {
        data: 'Graphic Design',
        src: 'images/gridfoto/12.jpg'
    }],

]


const SET_TIMEOUT = 3000;
function Gallery() {

    let count = 0;
    const galleryMenuWrapper = document.querySelector('.gallery-menu');
    const loadMore = document.querySelector('.btnl');
    const galleryContent = document.querySelector('.grid-foto');


    galleryMenuWrapper.addEventListener('click', (event) => {
        let dataMenu = null;
        const dataGallery = document.querySelectorAll('.grid_item-foto');

        if (event.target.nodeName === "BUTTON") {
            dataMenu = event.target.getAttribute('data-menu');

        }

        let menuItems = document.querySelectorAll('[data-menu]');

        menuItems.forEach(item => {
            if (item.classList.contains('active')) {
                item.classList.remove('active');
            }
        });

        if (!event.target.classList.contains('active')) {
            event.target.classList.add('active');
        }

        dataGallery.forEach((item) => {
            if (dataMenu !== item.getAttribute('data-gallery') && dataMenu !== 'all') {
                item.style.display = 'none'
            } else {
                item.style.display = 'block'
            }
        })
    })
    loadMore.addEventListener('click', () => {

        loadMore.classList.add('gallery_buttons');
        setTimeout(() => {
            loadMore.classList.remove('gallery_buttons');



            imag[count].forEach((image, id) => {
                galleryContent.insertAdjacentHTML('beforeend', `
              <div class="grid_item-foto" data-gallery="${image.data}"><img class="img${id + 13}" src="${image.src}" alt="photo">
                         <div class="imag-description">
                             <img src="img/icon.png" alt="icon">
                             <p class="title">creative design</p>
                             <p class="desc">Web Design</p>
                         </div>
                     </div>
`)
            })


            count++

            if ( count === imag.length ){
                loadMore.remove()
            }
        }, SET_TIMEOUT)

    })

}

document.addEventListener('DOMContentLoaded', () => {
    Gallery();
});


<!-- Initialize Swiper -->
const test = new Swiper(".thumbs-slider", {
    spaceBetween: 10,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesProgress: true,
});

const swiperMain = new Swiper(".slider", {
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-button-next ",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: test,
    },
});





