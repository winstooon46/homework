


let inputFirst = document.getElementById('firInput');
let inputSecond = document.getElementById('secInput');
const inputPass = document.querySelectorAll('.inputPass');
const btn = document.querySelector(".btn");
const p = document.createElement("p");
p.textContent = 'Нужно ввести одинаковые значения';
p.style.color = 'red';

inputPass.forEach((item)=> item.addEventListener('click', function () {
    const icon = this.nextElementSibling;
    icon.classList.toggle("fa-eye-slash");
    if (icon.className === 'fas fa-eye icon-password fa-eye-slash') {
        this.type = "text";
    } else {
        this.type = "password";
    }
}));

btn.addEventListener('click', function (e) {
    e.preventDefault();
    if(inputFirst.value === inputSecond.value && inputFirst.value !== '' && inputSecond.value !== ''){
        p.remove();
        alert('You are welcome');
    }else{
        btn.before(p);
    }
});