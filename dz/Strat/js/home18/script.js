
let myobj = {
    name: "Vova",
    surname: "Iaroshevskyi",
    job: "analyst",
    hzchto: [{a:1, b:2,},{c:3, d:4,}],
    flat: [1,2,3],
    getAge: function (){return 18},
    home: {
        a: 1,
        b: { c: [2,8,9],
             d: 2,},
    }
}

function cloneObject (obj){
    let res = {}
    for (const objKey in obj) {
        if (obj.hasOwnProperty(objKey)){
            if (Array.isArray(obj[objKey])) {
                res[objKey] = [];
                obj[objKey].forEach((el) => {res[objKey].push(el)})
            }
            else if (typeof obj[objKey] === "object" )  {
                 res[objKey] = cloneObject(obj[objKey])
            }
            else{
                res[objKey] = obj[objKey];
            }
        }

    }
    return res
}

console.log(myobj)
console.log(cloneObject(myobj))
