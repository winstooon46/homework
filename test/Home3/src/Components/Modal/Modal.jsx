// import {useState} from "react";
import React, {Component} from "react";
import "./Modal.scss"


// const Modal = () => {
//     let [openModal, setOpenModal]= useState(false)
//     return(
//         <>
//         <div  onClick={() => setOpenModal(openModal = !openModal)} className={`add-to-card ${openModal && 'active'}`}>Добавить в корзину</div>
//     {openModal &&(
//
//         <div className='modal active'>
//             <div className="modal__content active">
//             <h1 className="hedername"> Хотите добавить товар в корзину? <span className="icon" onClick={()=>setOpenModal(false)}>&#10006;</span></h1>
//             <div className="btns">
//
//                 <button className="ok"  >Yes</button>
//                 <button className="cancel" onClick={()=> setOpenModal(false)}>Cancel</button>
//             </div>
//         </div>
//
//         </div>
//     )}
//         </>
//
//     )}
//
//
// export default Modal

class Modal extends Component{

    render() {
        const {header,closeButton, text, actionOk} = this.props

 return(
            <>
                {/*<div  onClick={() => setOpenModal(openModal = !openModal)} className={`add-to-card ${openModal && 'active'}`}>Добавить в корзину</div>*/}
                {/*    {openModal &&(*/}

                        <div className='modal active' onClick={closeButton} >
                            <div className="modal__content active" onClick={e => e.stopPropagation()}>
                            <h1 className="hedername"> {header} <span className="icon" onClick={closeButton}>&#10006;</span></h1>
                                <h2 className="textBodu">{text}</h2>

                                <div className="btns">
                                <button className="ok" onClick={actionOk}>Yes</button>
                                <button className="cancel" onClick={closeButton}>Cancel</button>
                            </div>
                            </div>
                        </div>
            </>


        )
    }
}
export default Modal
