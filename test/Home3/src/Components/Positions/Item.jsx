import React, {Component} from 'react';
// import Modal from "../Modal/Modal";
import "./Positions.scss"

class Item extends Component {
    render() {
        const {clickModal} = this.props
        return (
            <div className='item'>
                <img src={"./chop/" + this.props.item.img} alt=""/>
                <h2>{this.props.item.title}  </h2>
                <p>{this.props.item.desc}</p>
                <b>{this.props.item.price}$</b>
                {/*<Modal/>*/}
                <div className='add-to-card active' onClick={clickModal}>Добавить в корзину</div>
            </div>
        );
    }
}


export default Item;