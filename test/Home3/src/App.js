// import React, {useState} from "react";
import React from "react";
import Header from "./Components/Header/indexs"
import Footer from "./Components/Footer/indexs"
import Positions from "./Components/Positions/indexs"
import './App.css'
import Modal from "./Components/Modal/Modal"


// import {Positions} from "./Components/Positions/Positions";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders:[],
            items: [],
            openModal: false,
        }
        this.addToOrder= this.addToOrder.bind(this)

    }





    componentDidMount() {
        fetch("nameItem.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    showModal = ()=> {
        // const {openModal} = this.state
        this.setState({openModal:true})
    }

    closeModal = ()=> {
        this.setState({openModal:false})
    }

    increaseItem = () => {
        const {itemF} = this.state
        this.setState({openModal:false})
        if(itemF >=1) return;
        // localStorage.setItem('user','user')
        this.setState({itemF: itemF + 1})
}
    decreaseItem = () => {
        const {itemF} = this.state;
        if(itemF <= 0) return;
        this.setState({itemF: itemF - 1})
    }




    render() {

        return (
            <>
            <div className="appClass">
                <Header/>
                <Positions items={this.state.items} onAdd = {this.addToOrder} clickModal={this.showModal} />
                <Footer/>
            </div>
        {this.state.openModal && <Modal header ="Добавить товар в корзину?" actionOk={this.increaseItem} closeButton = {this.closeModal}/> }
                {/*{this.state.openModal && <Modal header = "Удалить с корзины?" actionOktwo = {this.decreaseItem} closeButton = {this.closeModal}/> }*/}
       </> )
    }

    addToOrder(item){
        this.setState({orders: [...this.state.orders,item]})

    }
}


    export default App