import React, {useState} from 'react';

function App() {
  const [counter, setCounter] = useState(0)




    function increment() {
          setCounter(counter + 1)
    }


    function decrement() {
        setCounter(counter - 1)
    }

    return(
        <div>
         <h1>Счетчик: {counter}</h1>
         <button onClick={increment}>Добавить</button>
        <button onClick={decrement}>Убрать</button>
        </div>
    )
}


export default App